# Project

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

We have created a forum where users are required to register and login in order to view/write content.

Once logged in users can view the forum posts and the profile of the other forum members. Users can write new posts and comments ona topic and 

edit/delete their own posts/comments. Additional flag functionality exists for posts with inappropriate content. Users can also add/remove other 

users from their friend lists.

Admin users can in addition ban other users, thus restricting their rights to only viewing posts/comments/member list. An admin user can unflag an 

already flagged post or lock a post for future editing. 




## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
