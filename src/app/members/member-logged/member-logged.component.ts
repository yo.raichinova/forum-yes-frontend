import { Component, OnInit } from '@angular/core';
import { UserReturn } from 'src/app/models/user-return';
import { MembersDataService } from 'src/app/core/members-data.service';
import { AuthService } from 'src/app/core/auth.service';

@Component({
  selector: 'app-member-logged',
  templateUrl: './member-logged.component.html',
  styleUrls: ['./member-logged.component.scss']
})
export class MemberLoggedComponent implements OnInit {
  member: UserReturn;

  constructor(
    private membersDataService: MembersDataService,
    private authService: AuthService
  ) { }

  ngOnInit() {
    this.membersDataService.getMember(this.authService.retrieveMemberId()).subscribe((data: UserReturn) => {
      this.member = data;
    });
  }

}
