import { UserReturn } from '../models/user-return';
import { async } from 'q';
import { TestBed } from '@angular/core/testing';
import { MembersModule } from './members.module';
import { MembersDataService } from '../core/members-data.service';
import { Router, Data } from '@angular/router';
import { MembersComponent } from './members.component';
import { of, Subject } from 'rxjs';

describe('MembersComponent', () => {
   /*  let fixture;
    const membersDataService = jasmine.createSpyObj('MembersDataService', ['getAllMembers']);
    const router = jasmine.createSpyObj('Router', { navigate: of(['/members']) });

    const member: UserReturn = {
        id: 22,
        name: 'Michelle',
        email: 'mwdy@da.com',
        status: {
            isBanned: false,
            description: 'description',
        },
        posts: [],
        friends: [],
        postLikes: [1],
        postDislikes: [12],
        commentLikes: [22],
        commentDislikes: [5]
    };

    const members: UserReturn[] = [
        {
            id: 1,
            name: 'Yoda',
            email: 'yoda@yoda.com',
            status: {
                isBanned: false,
                description: 'description',
            },
            posts: [],
            friends: [],
            postLikes: [2],
            postDislikes: [5],
            commentLikes: [1],
            commentDislikes: [1]
        }];

    const mockObservable = new Subject();

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [],
            imports: [
                MembersModule,
            ],
            providers: [
                {
                    provide: MembersDataService,
                    useValue: {
                        membersDataService
                        // data: {
                        //     subscribe: (fn: (value: Data) => void) => fn([{ member }]),
                        // }
                    },
                },
                {
                    provide: Router,
                    useValue: router,
                }
            ]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });

    it('should create the MembersComponent', () => {
        fixture = TestBed.createComponent(MembersComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it('should obtain all members via the membersDataService upon initialization', async () => {
        mockObservable.next({ test: 'data' });
        fixture = TestBed.createComponent(MembersComponent);
        const app = fixture.debugElement.componentInstance;
        app.members = members;
        membersDataService.getAllMembers.and.returnValue(of(data => [{
            member }]));
        await fixture.detectChanges();
        // membersDataService.getAllMembers().subscribe((data: UserReturn) => expect(app.members).toBe(data));
        // membersDataService.getAllMembers().subscribe(
        //     membersData => {
        //         expect(membersData[0].id).toEqual(1);
        //         expect(membersData[0].name).toEqual('Yoda');
        //         expect(membersData[0].email).toEqual('yoda@yoda.com');
        //         expect(membersData[0].status).toEqual({ isBanned: false, description: 'description' });
        //         expect(membersData[0].posts).toEqual([]);
        //         expect(membersData[0].friends).toEqual([]);
        //         expect(membersData[0].postLikes).toEqual([2]);
        //         expect(membersData[0].postDislikes).toEqual([5]);
        //         expect(membersData[0].commentLikes).toEqual([1]);
        //         expect(membersData[0].commentDislikes).toEqual([1]);
        //     }
        // );
        expect(membersDataService.getAllMembers).toHaveBeenCalled();

    });

    it('should call getAllMembers', async () => {
        fixture = TestBed.createComponent(MembersComponent);
        const app = fixture.debugElement.componentInstance;
        app.getAllMembers();
        expect(membersDataService.getAllMembers).toHaveBeenCalled();
      });
    /*
        it('should call router.navigate', async () => {
            expect(router.navigate).toHaveBeenCalled();
        }); */
});
