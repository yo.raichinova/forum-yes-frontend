import { NgModule } from '@angular/core';
import { MembersComponent } from './members.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MembersRoutingModule } from './members-routing.module';
import { MemberProfileComponent } from './member-profile/member-profile.component';
import { MemberPreviewProfileComponent } from './member-preview-profile/member-preview-profile.component';
import { AddFriendComponent } from '../components/friends/add-friend/add-friend.component';
import { RemoveFriendComponent } from '../components/friends/remove-friend/remove-friend.component';

@NgModule({
  declarations: [
    MembersComponent,
    MemberPreviewProfileComponent,
    MemberProfileComponent,
    AddFriendComponent,
    RemoveFriendComponent,
  ],
  imports: [SharedModule, FormsModule, MembersRoutingModule]
})
export class MembersModule { }
