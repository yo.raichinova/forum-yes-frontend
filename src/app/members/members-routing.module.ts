import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MembersComponent } from './members.component';
import { MemberPreviewProfileComponent } from './member-preview-profile/member-preview-profile.component';
import { MemberProfileComponent } from './member-profile/member-profile.component';


const routes: Routes = [
    { path: '', component: MembersComponent, pathMatch: 'full' },
    { path: ':id', component: MemberPreviewProfileComponent },
    { path: 'profile', component: MemberProfileComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MembersRoutingModule { }
