import { MembersModule } from '../members.module';
import { MembersDataService } from 'src/app/core/members-data.service';
import { StorageService } from 'src/app/core/storage.service';
import { AuthService } from 'src/app/core/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { MemberPreviewProfileComponent } from './member-preview-profile.component';
import { TestBed, async } from '@angular/core/testing';
import { of, throwError } from 'rxjs';
import { UserReturn } from 'src/app/models/user-return';

describe('MemberPreviewProfileComponent', () => {
    let fixture;
    const membersDataService = jasmine.createSpyObj('MembersDataService', [
        'getMember',
        'addFriendById',
        'unfriendById',
        'banMember',
        'deleteMember']);
    const storageService = jasmine.createSpyObj('StorageService', ['getItem']);
    const authService = jasmine.createSpyObj('AuthService', ['getUserById']);
    const toastr = jasmine.createSpyObj('ToastrService', ['success', 'error']);
    const router = jasmine.createSpyObj('Router', ['navigate']);
    const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['params']);

    const member: UserReturn = {
        id: 22,
        name: 'Michelle',
        email: 'mwdy@da.com',
        status: {
            isBanned: false,
            description: 'description',
        },
        posts: [],
        friends: [],
        postLikes: [1],
        postDislikes: [12],
        commentLikes: [22],
        commentDislikes: [5]
    };

    const currentlyLoggedMember: UserReturn = {
        id: 2,
        name: 'Kay',
        email: 'Kay@da.com',
        status: {
            isBanned: false,
            description: 'description',
        },
        posts: [],
        friends: [],
        postLikes: [3],
        postDislikes: [2],
        commentLikes: [2],
        commentDislikes: [4]
    };

    const mockActivatedRoute = {
        params: of({ id: 1 })
    };

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [],
            imports: [
                MembersModule,
            ],
            providers: [
                {
                    provide: MembersDataService,
                    useValue: membersDataService,
                },
                {
                    provide: StorageService,
                    useValue: storageService,
                },
                {
                    provide: AuthService,
                    useValue: authService,
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                {
                    provide: Router,
                    useValue: router,
                },
                {
                    provide: ActivatedRoute,
                    useValue: mockActivatedRoute,
                }
            ]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });

    it('should create the MemberPreviewProfileComponent', () => {
        fixture = TestBed.createComponent(MemberPreviewProfileComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it('should obtain logged user by invoking getUserById from the authService upon initialization', async () => {
        fixture = TestBed.createComponent(MemberPreviewProfileComponent);
        const app = fixture.debugElement.componentInstance;
        authService.getUserById.and.returnValue(of(currentlyLoggedMember));
        membersDataService.getMember.and.returnValue(of(member));
        await fixture.detectChanges();
        expect(app.currentlyLoggedMember).toBe(currentlyLoggedMember);
    });

    it('should obtain specific user by invoking getMember from the membersDataService upon initialization', async () => {
        fixture = TestBed.createComponent(MemberPreviewProfileComponent);
        const app = fixture.debugElement.componentInstance;
        activatedRoute.params.and.returnValue(of(mockActivatedRoute));
        authService.getUserById.and.returnValue(of(currentlyLoggedMember));
        membersDataService.getMember.and.returnValue(of(member));
        await fixture.detectChanges();
        expect(app.member.name).toEqual(member.name);
    });

    it('addFriendById method should call toastr.success when a member was added as a friend successfully', async () => {

        fixture = TestBed.createComponent(MemberPreviewProfileComponent);
        const app = fixture.debugElement.componentInstance;
        activatedRoute.params.and.returnValue(of(mockActivatedRoute));
        membersDataService.getMember.and.returnValue(of(member));
        membersDataService.addFriendById.and.returnValue(of({}));
        await fixture.detectChanges();

        app.friendMember();

        expect(app.showUnfriendButton).toBe(true);
        expect(app.showFriendButton).toBe(false);
        expect(toastr.success).toHaveBeenCalledTimes(1);
        expect(toastr.success).toHaveBeenCalledWith('Michelle has been added to your friend\'s list.');
    });

    it('addFriendById method should call toastr.error when a member was not added as a friend successfully', async () => {
        fixture = TestBed.createComponent(MemberPreviewProfileComponent);
        const app = fixture.debugElement.componentInstance;
        membersDataService.getMember.and.returnValue(of(member));
        membersDataService.addFriendById.and.returnValue(throwError({ message: `test` }));
        await fixture.detectChanges();

        app.friendMember();

// tslint:disable-next-line: no-unused-expression
        expect(app.showUnfriendButton).toBeUndefined;
// tslint:disable-next-line: no-unused-expression
        expect(app.showFriendButton).toBeUndefined;
        expect(toastr.error).toHaveBeenCalledTimes(1);
        expect(toastr.error).toHaveBeenCalledWith('Unable to add friend');
    });
});
