import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserReturn } from 'src/app/models/user-return';
import { Subscription } from 'rxjs';
import { MembersDataService } from 'src/app/core/members-data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Posst } from 'src/app/models/post';
import { Friend } from 'src/app/models/friend';
import { AuthService } from 'src/app/core/auth.service';
import { ToastrService } from 'ngx-toastr';
import { StorageService } from 'src/app/core/storage.service';
import { Status } from 'src/app/models/status';

@Component({
  selector: 'app-member-preview-profile',
  templateUrl: './member-preview-profile.component.html',
  styleUrls: ['./member-preview-profile.component.scss']
})
export class MemberPreviewProfileComponent implements OnInit, OnDestroy {

  paramsSubscription: Subscription;
  member: UserReturn;
  posts: Posst[] = [];
  friends: Friend[] = [];
  currentlyLoggedMember: UserReturn;
  loggedUserFriends: Friend[];
  status: Status;
  loggedMemberRole: string;
  loggedMemberName: string;
  loggedMemberId: string;
  description = 'This member has violated the rules.';
  showDeleteButton = false;
  showBanButton = false;
  showFriendButton: boolean;
  showUnfriendButton: boolean;

  public constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly membersDataService: MembersDataService,
    private readonly storageService: StorageService,
    private readonly authService: AuthService,
    private readonly toastr: ToastrService,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.loggedMemberRole = this.storageService.getItem('role');
    this.loggedMemberName = this.storageService.getItem('username');
    this.loggedMemberId = this.storageService.getItem('id');

    this.paramsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.authService.getUserById(this.loggedMemberId).subscribe((user: UserReturn) => {
          this.currentlyLoggedMember = user;
          this.loggedUserFriends = user.friends;

          this.membersDataService.getMember(+params.id).subscribe((data: UserReturn) => {
            this.member = data;
            this.posts = this.member.posts;
            this.friends = this.member.friends;
            this.status = this.member.status;

            if (this.loggedMemberRole === 'admin') {
              this.showDeleteButton = true;
              this.showBanButton = true;
            }
            if (this.member.status.isBanned === true) {
              this.showBanButton = false;
            }
            if (this.member.name === this.loggedMemberName) {
              this.showDeleteButton = false;
              this.showBanButton = false;
              this.showUnfriendButton = false;
              this.showFriendButton = false;

            } else if (this.loggedUserFriends.find(friend => friend.name === this.member.name)) {
              this.showUnfriendButton = true;
              this.showFriendButton = false;
            } else {
              this.showFriendButton = true;
              this.showUnfriendButton = false;
            }
          });
        });
      }
    );
  }

  friendMember() {
    this.membersDataService.addFriendById(this.currentlyLoggedMember, this.member.id).subscribe(
      () => {
        this.showUnfriendButton = true;
        this.showFriendButton = false;
        this.toastr.success(`${this.member.name} has been added to your friend's list.`);
      },
      error => {
        this.toastr.error(`Unable to add friend`);
      }
    );
  }

  unfriendMember(): void {
    this.membersDataService.unfriendById(this.currentlyLoggedMember, this.member.id).subscribe(
      () => {
        this.showFriendButton = true;
        this.showUnfriendButton = false;
        this.toastr.success(`${this.member.name} has been removed from your friends' list.`);
      },
      () => {
        this.toastr.error(`Unable to remove friend from friends' list.`);
      }
    );
  }

  hasFriends() {
    if (!this.friends.toString()) {
      return false;
    }
    return true;
  }

  banMember() {
    const descriptionObject = { content: this.description };
    this.membersDataService.banMember(descriptionObject, this.member.id).subscribe(
      () => {
        this.showBanButton = false;
        this.toastr.success(`This member's ban status has been updated successfully!`);
        this.router.navigate(['/members']);
      },
      (error) => {
        this.toastr.error(error.message);
      }
    );
  }

  deleteMember() {
    this.membersDataService.deleteMember(this.member.id).subscribe(
      () => {
        this.toastr.success('Member has been deleted successfully!');
        this.router.navigate(['/members']);
      },
      (error) => {
        this.toastr.error(error.message);
      }
    );
  }

  ngOnDestroy(): void {
    this.paramsSubscription.unsubscribe();
  }

}
