import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { UserReturn } from '../models/user-return';
import { Subscription } from 'rxjs';
import { MembersDataService } from '../core/members-data.service';
import { SearchService } from '../core/search.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss']
})
export class MembersComponent implements OnInit, OnDestroy {

  public members: UserReturn[] = [];

  @Input()
  public member: UserReturn;

  private searchSubscription: Subscription;

  public constructor(
    private readonly membersDataService: MembersDataService,
    private readonly router: Router
  ) { }

  public ngOnInit(): void {
    this.searchSubscription = this.membersDataService
      .getAllMembers()
      .subscribe((data: UserReturn[]) => {
        this.members = data;
      });
  }


  redirectToMemberPreview(): void {
    this.router.navigate(['/members/${this.member.id}']);
  }

  public ngOnDestroy(): void {
    this.searchSubscription.unsubscribe();
  }

}
