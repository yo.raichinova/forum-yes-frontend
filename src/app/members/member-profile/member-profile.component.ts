import { Component, OnInit, Input } from '@angular/core';
import { UserReturn } from 'src/app/models/user-return';
import { AuthService } from 'src/app/core/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { MembersDataService } from 'src/app/core/members-data.service';
import { UpdateProfile } from 'src/app/models/update-profile';

@Component({
  selector: 'app-member-profile',
  templateUrl: './member-profile.component.html',
  styleUrls: ['./member-profile.component.scss']
})
export class MemberProfileComponent implements OnInit {
  member: UserReturn;
  memberUpdate: UpdateProfile;

  constructor(
    private membersDataService: MembersDataService,
    private authService: AuthService,
    private toastr: ToastrService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.membersDataService.getMember(this.authService.retrieveMemberId()).subscribe((data: UserReturn) => {
      this.member = data;
    });
  }

  editUser() {
    this.membersDataService.editMemberById(this.member.id, this.memberUpdate).subscribe(() => {
      this.toastr.success('Profile changes have been updated successfully');
      // sessionStorage.clear();
      localStorage.clear();
      this.router.navigate(['/login']);
    });
  }

}
