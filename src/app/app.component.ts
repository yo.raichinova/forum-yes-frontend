import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from './core/auth.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent implements OnInit, OnDestroy {

  public username = '';
  public isLoggedIn = false;
  private isLoggedInSubscription: Subscription;

  constructor(
    private readonly authService: AuthService,
    private toastr: ToastrService,
    private readonly router: Router,
    private readonly jwtHelper: JwtHelperService,
  ) {}

  public ngOnInit(): void {

    this.isLoggedInSubscription = this.authService.isLoggedIn$.subscribe(
      username => {
        if (username === null) {
          this.username = '';
          this.isLoggedIn = false;
        } else {
          this.username = username;
          this.isLoggedIn = true;
        }
      }
    );
  }

  public ngOnDestroy(): void {
    this.isLoggedInSubscription.unsubscribe();
  }

  public logout(): void {
    this.authService
      .logout()
      .subscribe(
        () => {
          this.toastr.success('Successful logout!'),
            this.router.navigate(['users/login']);
        },
        () => this.toastr.error('Logout failed!')
      );
  }
}
