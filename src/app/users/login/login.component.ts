import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserLogin } from 'src/app/models/user-login';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly toastr: ToastrService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) { }

  public ngOnInit() {
    this.loginForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required]]
    });
  }

  public login() {
    const user: UserLogin = this.loginForm.value;

    this.authService.login(user).subscribe(
      (data) => {
        this.toastr.success(`Successful login!`);
        this.router.navigate(['/home']);
        if (data.user.status.isBanned) {
          this.toastr.error('Your profile has been banned. You can only read content in the forum.');
        }
      },
      () => {
        this.toastr.error('Login failed!');
      }
    );
  }
}
