import { LoginComponent } from './login.component';
import { ComponentFixture, async, TestBed, inject } from '@angular/core/testing';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SharedModule } from 'src/app/shared/shared.module';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';
import { AppModule } from 'src/app/app.module';
import { UsersModule } from '../users.module';

describe('LoginComponent', () => {
    let app: LoginComponent;
    let fixture: ComponentFixture<LoginComponent>;
    const formBuilder: FormBuilder = new FormBuilder();
    const router = jasmine.createSpyObj('Router', ['navigate']);
    const authService = jasmine.createSpyObj('AuthService', ['login']);
    const toastr = jasmine.createSpyObj('ToastrService', ['success', 'error']);


    beforeEach((() => {
        TestBed.configureTestingModule({
            declarations: [],
            imports: [SharedModule,
                ReactiveFormsModule,
                AppModule,
                FormsModule,
                UsersModule
            ],
            providers: [
                {
                    provide: AuthService,
                    useValue: authService,
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                {
                    provide: Router,
                    useValue: router,
                },
                {
                    provide: FormBuilder,
                    useValue: formBuilder,
                }
            ]
        });
    }));

    beforeEach(async () => {
        fixture = TestBed.createComponent(LoginComponent);
        app = fixture.componentInstance;

        await fixture.detectChanges();
    });
   
    it('should be created', () => {
        expect(app).toBeTruthy();
    });

});
