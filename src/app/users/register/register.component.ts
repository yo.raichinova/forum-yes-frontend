import { UserRegister } from './../../models/user-register';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/core/auth.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerForm: FormGroup;

  constructor(
    private readonly authService: AuthService,
    private readonly toastr: ToastrService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) { }

  public ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.minLength(4), Validators.email]],
      password: ['', [Validators.required]]
    });
  }

  public register() {
    const user: UserRegister = this.registerForm.value;
    this.authService.register(user).subscribe(
      (result) => {
        const loginUser = { name: user.name, password: user.password };
        this.authService.login(loginUser).subscribe(
          response => {
            this.toastr.success(`Welcome, ${response.user.name}!`);
            this.router.navigate(['/home']);
          },
          error => this.toastr.error(error.message),
        );
      },
      (error) => this.toastr.error(error.message),
    );
  }
}
