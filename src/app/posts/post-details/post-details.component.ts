import { Component, OnInit, OnDestroy } from '@angular/core';
import { Posst } from '../../../app/models/post';
import { Comment } from '../../../app/models/comment';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { PostsDataService } from '../../../app/core/post-data.service';
import { StorageService } from '../../../app/core/storage.service';
import { ToastrService } from 'ngx-toastr';
import { PostsActionsService } from '../../../app/core/post-actions.service';
import { AuthService } from '../../../app/core/auth.service';
import { UserReturn } from '../../../app/models/user-return';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.scss']
})
export class PostDetailsComponent implements OnInit {

  public post: Posst;
  public postId: number;
  public author: string;
  public loggedUserName: string;
  public loggedUserRole: string;
  public loggedUserId: string;
  public loggedUser: UserReturn;
  public showEditButton = false;
  public showVotesIcons = true;
  public showFlagIcon = true;
  public showLockIcon = false;
  public disableLikeButton = false;
  public disableDislikeButton = false;
  public comments: Comment[];
  public hasComments: boolean;
  public routeParamsSubscription: Subscription;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly postDataService: PostsDataService,
    private readonly postActionsService: PostsActionsService,
    private readonly authService: AuthService,
    private readonly storageService: StorageService,
    private readonly toastr: ToastrService,
    private readonly router: Router,

  ) { }

  public ngOnInit(): void {
    this.loggedUserName = this.storageService.getItem('username');
    this.loggedUserRole = this.storageService.getItem('role');
    this.loggedUserId = this.storageService.getItem('id');

    this.routeParamsSubscription = this.activatedRoute.params.subscribe(
      params => {
        this.postId = +params.id;
        this.postDataService.getPost(this.postId).subscribe((data: Posst) => {
          this.post = data;
          this.author = this.post.user;

          if (this.author === this.loggedUserName) {
            this.showEditButton = true;
            this.showVotesIcons = false;
            this.showFlagIcon = false;
          }
          if (this.loggedUserRole === 'admin') {
            this.showEditButton = true;
            this.showLockIcon = true;
          }
          if (this.loggedUserRole === 'member' && this.post.isFlagged === true) {
            this.showFlagIcon = false;
          }
          this.comments = this.post.comments;
          if (this.comments.length > 0) {
            this.hasComments = true;
          } else {
            this.hasComments = false;
          }
        });

        this.authService.getUserById(this.loggedUserId).subscribe(
          (user) => {
            this.loggedUser = user;
            if (user.postLikes.includes(this.postId)) {
              this.disableLikeButton = true;
            }
            if (user.postDislikes.includes(this.postId)) {
              this.disableDislikeButton = true;
            }
          },
          () => {
            this.router.navigate(['login']);
          }
        );
      }
    );
  }

  public triggerDelete() {
    this.postDataService.deletePost(this.postId).subscribe(
      (data) => {
        this.toastr.success(`${data.message}`);
        this.router.navigate(['/posts']);
      },
      (error) => {
        this.toastr.error('Delete unsuccessful');
      }
    );
  }

  public triggerUpvote() {
    this.postActionsService.upvotePost(this.postId).subscribe(
      () => {
        this.post.likedBy++;
        if (this.disableDislikeButton) {
          this.post.dislikedBy--;
          this.disableDislikeButton = false;
        }
        this.disableLikeButton = true;
      },
      (error) => {
        this.toastr.error('Vote unsuccessful');
      }
    );
  }

  public triggerDownvote() {
    this.postActionsService.downvotePost(this.postId).subscribe(
      () => {
        this.post.dislikedBy++;
        if (this.disableLikeButton) {
          this.post.likedBy--;
          this.disableLikeButton = false;
        }
        this.disableDislikeButton = true;
      },
      (error) => {
        this.toastr.error('Vote unsuccessful');
      }
    );
  }

  public triggerLockPost() {
    this.postActionsService.lockPost(this.postId).subscribe(
      () => {
        this.post.isLocked = !this.post.isLocked;
        if (this.showLockIcon) {
          this.showLockIcon = false;
        }
        this.showLockIcon = true;
      },
      (error) => {
        this.toastr.error('Lock unsuccessful');
      }
    );
  }

  public triggerFlagPost() {
    this.postActionsService.flagPost(this.postId).subscribe(
      () => {
        this.post.isFlagged = !this.post.isFlagged;
      },
      (error) => {
        this.toastr.error('Flag unsuccessful');
      }
    );
  }
}
