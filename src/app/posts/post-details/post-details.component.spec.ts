import { UserReturn } from 'src/app/models/user-return';
import { Subscription, of, BehaviorSubject } from 'rxjs';
import { async } from 'q';
import { TestBed } from '@angular/core/testing';
import { PostsModule } from '../posts.module';
import { PostsDataService } from 'src/app/core/post-data.service';
import { StorageService } from 'src/app/core/storage.service';
import { AuthService } from 'src/app/core/auth.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PostDetailsComponent } from './post-details.component';
import { Posst } from 'src/app/models/post';
import { PostsActionsService } from 'src/app/core/post-actions.service';

describe('PostDetailsComponent', () => {
    // let fixture;
    // const postsDataService = jasmine.createSpyObj('PostsDataService', [
    //     'getPost',
    //     'deletePost',
    // ]);
    // const postsActionsService = jasmine.createSpyObj('PostsActionsService', [
    //     'upvotePost',
    //     'downvotePost',
    //     'lockPost',
    //     'flagPost'
    // ]);
    // const storageService = jasmine.createSpyObj('StorageService', ['getItem']);
    // const authService = jasmine.createSpyObj('AuthService', ['getUserById']);
    // const toastr = jasmine.createSpyObj('ToastrService', ['success', 'error']);
    // const router = jasmine.createSpyObj('Router', ['navigate']);
    // const activatedRoute = jasmine.createSpyObj('ActivatedRoute', ['params']);

    // const post: Posst = {
    //     id: 1,
    //     title: 'testTitle',
    //     content: 'testContent',
    //     user: 'teddy',
    //     createdAt: new Date(),
    //     likedBy: 2,
    //     dislikedBy: 1,
    //     comments: [],
    //     isFlagged: false,
    //     isLocked: false
    // };
    // const postId = 2;
    // const author = 'testAuthor';
    // const loggedUsername = 'username';
    // const loggedUserRole = 'role';
    // const loggedUserId = 'id';
    // const loggedUser: UserReturn = {
    //     id: 2,
    //     name: 'Kay',
    //     email: 'Kay@da.com',
    //     status: {
    //         isBanned: false,
    //         description: 'description',
    //     },
    //     posts: [],
    //     friends: [],
    //     postLikes: [3],
    //     postDislikes: [2],
    //     commentLikes: [2],
    //     commentDislikes: [4]
    // };
    // const comments: Comment[] = [];

    // beforeEach(async(() => {
    //     TestBed.configureTestingModule({
    //         declarations: [],
    //         imports: [
    //             PostsModule,
    //         ],
    //         providers: [
    //             {
    //                 provide: PostsDataService,
    //                 useValue: postsDataService,
    //             },
    //             {
    //                 provide: PostsActionsService,
    //                 useValue: postsActionsService,
    //             },
    //             {
    //                 provide: StorageService,
    //                 useValue: storageService,
    //             },
    //             {
    //                 provide: AuthService,
    //                 useValue: authService,
    //             },
    //             {
    //                 provide: ToastrService,
    //                 useValue: toastr,
    //             },
    //             {
    //                 provide: Router,
    //                 useValue: router,
    //             },
    //             {
    //                 provide: ActivatedRoute,
    //                 useValue: {
    //                     params: new BehaviorSubject({ id: 1 })
    //                     /* data: {
    //                         subscribe: (fn: (value: Params) => void) => fn({
    //                             id: '1',
    //                         })
    //                     } */
    //                 }
    //             }
    //         ]
    //     });
    // }));

    // afterEach(() => {
    //     if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
    //         (fixture.nativeElement as HTMLElement).remove();
    //     }
    // });

    // it('should create the PostDetailsComponent', () => {
    //     fixture = TestBed.createComponent(PostDetailsComponent);
    //     const app = fixture.debugElement.componentInstance;
    //     expect(app).toBeTruthy();
    // });

    // it('should obtain the postId from the route params upon initialization', async () => {

    //     fixture = TestBed.createComponent(PostDetailsComponent);
    //     const app = fixture.debugElement.componentInstance;

    //     storageService.getItem.and.returnValue('username');
    //     storageService.getItem.and.returnValue('role');
    //     storageService.getItem.and.returnValue('id');
    //     activatedRoute.params.and.returnValue(of({ id: 1 }));
    //     postsDataService.getPost.and.returnValue(of(post));
    //     authService.getUserById.and.returnValue(of(loggedUser));
    //     await fixture.detectChanges();
    //     // expect(storageService.getItem).toHaveBeenCalledTimes(3);
    //     // expect(activatedRoute.params).toHaveBeenCalled();
    //     // expect(postsDataService.getPost).toHaveBeenCalled();
    //     // expect(authService.getUserById).toHaveBeenCalled();
    //     expect(loggedUsername).toBe('username');
    //     expect(loggedUserRole).toBe('role');
    //     expect(loggedUserId).toBe('id');
    //     expect(app.postId).toBe(1);
    //     expect(post).toBe(post);
    //     expect(loggedUser).toBe(loggedUser);
    // });
});
