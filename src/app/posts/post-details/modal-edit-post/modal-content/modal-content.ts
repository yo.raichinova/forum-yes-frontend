import { Component, Input, OnInit } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { PostsDataService } from '../../../../core/post-data.service';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-ngbd-modal-content',
  template: `
<form [formGroup]="this.formGroup" (validSubmit)="onSubmit(this.formGroup.value, this.formGroup)">
  <div class="modal-header">
    <h4 class="modal-title">Here you can edit your post!</h4>
    <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
      <div>
        <label for="title">Title</label>
        <input  [value]="postTitle" type="text" class="form-control" id="title-input" placeholder="Title" formControlName="title">
      </div>
        <div *ngIf="this.formGroup.controls.title.invalid && this.formGroup.controls.title.dirty" class="alert alert-danger">
          <div *ngIf="this.formGroup.controls.title.errors.required">
            You cannot send a post without a title!
          </div>
          <div *ngIf="this.formGroup.controls.title.errors.minlength">
            The title must be at least 2 characters long.
          </div>
        </div>
    <div>
      <label for="content" class="pt-2">Content</label>
      <textarea [value]="postContent" type="text" class="form-control"
      id="content-input" placeholder="Content" rows="3" formControlName="content"></textarea>
    </div>
      <div *ngIf="this.formGroup.controls.content.invalid && this.formGroup.controls.content.dirty" class="alert alert-danger">
          <div *ngIf="this.formGroup.controls.content.errors.required">
            You cannot send a post without content!
          </div>
          <div *ngIf="this.formGroup.controls.content.errors.minlength">
            The content must be at least 2 characters long.
          </div>
      </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-outline-success">Submit</button>
    <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
  </div>
</form>
  `
})
export class AppNgbdModalEditContentComponent implements OnInit {

  @Input() id: number;
  @Input() postTitle: string;
  @Input() postContent: string;
  public formGroup: FormGroup;
  public routeParamsSubscription: Subscription;


  constructor(
    public activeModal: NgbActiveModal,
    private readonly postDataService: PostsDataService,
    private readonly toastr: ToastrService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder,
    private readonly activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      title: new FormControl(
        this.postTitle,
        [
          Validators.required,
          Validators.minLength(2),
        ]
      ),
      content: new FormControl(
        this.postContent,
        [
          Validators.required,
          Validators.minLength(2)
        ]),
    });
  }

  public onSubmit(post, input) {
    this.activeModal.close('Submit');
    this.postDataService.updatePost(this.id, post).subscribe(
      (result) => {
        this.toastr.success(`${result.message}`);
        setTimeout(() => location.reload(), 1500);
      },
      () => {
        this.toastr.error('Post could not be edited.');
        this.router.navigate(['/posts', this.id]);
      }
    );
  }
}
