import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppNgbdModalEditContentComponent } from './modal-content/modal-content';

@Component({
  selector: 'app-ngbd-modal-edit-component',
  templateUrl: './modal-edit-component.html'
})
export class AppNgbdModalPostEditComponent {
  @Input() id: number;
  @Input() postTitle: string;
  @Input() postContent: string;

  constructor(private modalService: NgbModal) { }

  open() {
    const modalRef = this.modalService.open(AppNgbdModalEditContentComponent);
    modalRef.componentInstance.id = this.id;
    modalRef.componentInstance.postTitle = this.postTitle;
    modalRef.componentInstance.postContent = this.postContent;
  }
}
