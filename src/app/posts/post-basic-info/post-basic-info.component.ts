import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { Posst } from '../../../app/models/post';

@Component({
  selector: 'app-post-basic-info',
  templateUrl: './post-basic-info.component.html',
  styleUrls: ['./post-basic-info.component.scss']
})
export class PostBasicInfoComponent {

  @Input() public post: Posst;
  @Input() public showEditButton: boolean;
  @Input() public showVotesIcons: boolean;
  @Input() public showFlagIcon: boolean;
  @Input() public isPostFlagged: boolean;
  @Input() public showLockIcon: boolean;
  @Input() public isLikeIconDisabled: boolean;
  @Input() public isDislikeIconDisabled: boolean;

  @Output() public deletePost = new EventEmitter<undefined>();
  @Output() public upvotePost = new EventEmitter<undefined>();
  @Output() public downvotePost = new EventEmitter<undefined>();
  @Output() public lockPost = new EventEmitter<undefined>();
  @Output() public flagPost = new EventEmitter<undefined>();

  public onDeletePost(): void {
    this.deletePost.emit();
  }
  public onUpvotePost(): void {
    this.upvotePost.emit();
  }
  public onDownvotePost(): void {
    this.downvotePost.emit();
  }

  public onLockPost(): void {
    this.lockPost.emit();
  }

  public onFlagPost(): void {
    this.flagPost.emit();
  }
}
