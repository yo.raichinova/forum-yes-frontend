import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { PostsComponent } from './posts.component';
import { PostDetailsComponent } from './post-details/post-details.component';

const routes: Routes = [
    { path: '', component: PostsComponent, pathMatch: 'full' },
    { path: ':id', component: PostDetailsComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PostsRoutingModule { }
