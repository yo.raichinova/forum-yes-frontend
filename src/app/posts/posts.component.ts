import { Component, OnInit, OnDestroy } from '@angular/core';
import { Posst } from '../models/post';
import { Subscription } from 'rxjs';
import { PostsDataService } from '../core/post-data.service';
import { SearchService } from '../core/search.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.scss']
})
export class PostsComponent implements OnInit, OnDestroy {

  public posts: Posst[] = [];

  private searchSubscription: Subscription;

  public constructor(
    private readonly postsDataService: PostsDataService,
    private readonly searchService: SearchService
  ) { }

  public ngOnInit(): void {
    this.searchSubscription = this.searchService.search$.subscribe(
      (phrase: string) => {
        this.postsDataService
          .getAllPosts(phrase)
          .subscribe((data: Posst[]) => {
            this.posts = data;
          });
      }
    );
  }

  public ngOnDestroy(): void {
    this.searchSubscription.unsubscribe();
  }
}
