import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppNgbdModalCommentCreateContentComponent } from './modal-content/modal-content';

@Component({
  selector: 'app-ngbd-modal-comment-create-component',
  templateUrl: './modal-component.html'
})
export class AppNgbdModalCommentCreateComponent {

  @Input() public postId: number;

  constructor(private modalService: NgbModal) { }

  open() {
    const modalRef = this.modalService.open(AppNgbdModalCommentCreateContentComponent);
    modalRef.componentInstance.postId = this.postId;
  }
}
