import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { PostsComponent } from './posts.component';
import { PostsRoutingModule } from './posts-routing.module';
import { PostDetailsComponent } from './post-details/post-details.component';
import { AppNgbdModalComponent } from './modal-create-post/modal-component';
import { AppNgbdModalContentComponent } from './modal-create-post/modal-content/modal-content';
import { AppNgbdModalPostEditComponent } from './post-details/modal-edit-post/modal-edit-component';
import { AppNgbdModalEditContentComponent } from './post-details/modal-edit-post/modal-content/modal-content';
import { PostBasicInfoComponent } from './post-basic-info/post-basic-info.component';
import { AppNgbdModalCommentCreateContentComponent } from './modal-create-comment/modal-content/modal-content';
import { AppNgbdModalCommentCreateComponent } from './modal-create-comment/modal-component';

@NgModule({
  declarations: [
    PostsComponent,
    PostDetailsComponent,
    AppNgbdModalComponent,
    AppNgbdModalContentComponent,
    AppNgbdModalPostEditComponent,
    AppNgbdModalEditContentComponent,
    AppNgbdModalCommentCreateComponent,
    AppNgbdModalCommentCreateContentComponent,
    PostBasicInfoComponent,
  ],
  imports: [SharedModule, ReactiveFormsModule, PostsRoutingModule],
  entryComponents: [AppNgbdModalContentComponent, AppNgbdModalEditContentComponent, AppNgbdModalCommentCreateContentComponent],
})

export class PostsModule { }
