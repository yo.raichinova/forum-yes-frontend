import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { PostsDataService } from '../../../core/post-data.service';
import { CreatePost } from '../../../../app/models/create-post';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ngbd-modal-content',
  template: `
    <form [formGroup]="this.formGroup" (validSubmit)="onSubmit(this.formGroup.value)">
  <div class="modal-header">
    <h4 class="modal-title">Please write your post here!</h4>
    <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div>
      <label for="title">Title</label>
      <input type="text" class="form-control" id="title-input" placeholder="Title" formControlName="title">
    </div>
        <div *ngIf="this.formGroup.controls.title.invalid && this.formGroup.controls.title.dirty" class="alert alert-danger">
          <div *ngIf="this.formGroup.controls.title.errors.required">
            You cannot send a post without a title!
          </div>
          <div *ngIf="this.formGroup.controls.title.errors.minlength">
            The title must be at least 2 characters long.
          </div>
        </div>
    <div>
      <label for="content" class="pt-2">Content</label>
      <textarea type="text" class="form-control" id="content-input" rows="3" placeholder="Content" formControlName="content"></textarea>
    </div>
        <div *ngIf="this.formGroup.controls.content.invalid && this.formGroup.controls.content.dirty" class="alert alert-danger">
          <div *ngIf="this.formGroup.controls.content.errors.required">
            You cannot send a post without content!
          </div>
          <div *ngIf="this.formGroup.controls.content.errors.minlength">
            The content must be at least 2 characters long.
          </div>
      </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-outline-success">Submit</button>
    <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
  </div>
</form>
  `
})
export class AppNgbdModalContentComponent implements OnInit {

  public formGroup: FormGroup;

  constructor(
    public activeModal: NgbActiveModal,
    private readonly postDataService: PostsDataService,
    private readonly toastr: ToastrService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.formGroup = new FormGroup({
      title: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(2),
        ]
      ),
      content: new FormControl(
        '',
        [
          Validators.required,
          Validators.minLength(2)
        ]),
    });
  }

  onSubmit(post: CreatePost) {
    this.activeModal.close('Submit');
    this.postDataService.createPost(post).subscribe(
      (createdPost) => {
        const id = createdPost.id;
        this.toastr.success('Post successfully submitted!');
        this.router.navigate(['/posts', id]);
      },
      () => {
        this.toastr.error('Post could not be submitted.');
        this.router.navigate(['/posts']);
      }
    );
  }
}
