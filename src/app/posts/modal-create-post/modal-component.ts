import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppNgbdModalContentComponent } from './modal-content/modal-content';

@Component({
  selector: 'app-ngbd-modal-component',
  templateUrl: './modal-component.html'
})
export class AppNgbdModalComponent {
  constructor(private modalService: NgbModal) { }

  open() {
    const modalRef = this.modalService.open(AppNgbdModalContentComponent);
  }
}
