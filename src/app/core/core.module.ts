
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { AuthService } from './auth.service';
import { StorageService } from './storage.service';
import { SearchService } from './search.service';
import { PostsDataService } from './post-data.service';
import { MembersDataService } from './members-data.service';
import { ToastrService } from 'ngx-toastr';
import { PostsActionsService } from './post-actions.service';
import { CommentsDataService } from './comment-data.service';
import { CommentsActionsService } from './comment-actions.service';

@NgModule({
  providers: [
    AuthService,
    StorageService,
    SearchService,
    PostsDataService,
    MembersDataService,
    ToastrService,
    PostsActionsService,
    MembersDataService,
    CommentsDataService,
    CommentsActionsService,
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
