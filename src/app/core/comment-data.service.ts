import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateComment } from '../models/create-comment';
import { SharedModule } from '../shared/shared.module';
import { CoreModule } from './core.module';

@Injectable({
    providedIn: 'root'
})

export class CommentsDataService {
    public constructor(private readonly http: HttpClient) { }

    public createComment(postId: number, comment: CreateComment): Observable<Comment> {
        return this.http.post<Comment>(`http://localhost:3002/posts/${postId}/comments`, comment);
    }

    public updateComment(postId: number, commentId: number, comment: CreateComment): Observable<{ message: string }> {
        return this.http.put<{ message: string }>(`http://localhost:3002/posts/${postId}/comments/${commentId}`, comment);
    }
    public deleteComment(postId: number, commentId: number): Observable<{ message: string }> {
        return this.http.delete<{ message: string }>(`http://localhost:3002/posts/${postId}/comments/${commentId}`);
    }

}
