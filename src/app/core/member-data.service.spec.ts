import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';
import { MembersDataService } from './members-data.service';
import { of } from 'rxjs';
import { UserReturn } from '../models/user-return';
import { UpdateProfile } from '../models/update-profile';
import { BanDescription } from '../models/ban-description';

describe('MembersDataService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

    const testMembers: UserReturn[] = [
        {
            id: 1,
            name: 'Yoda',
            email: 'yoda@yoda.com',
            status: {
                isBanned: false,
                description: 'description',
            },
            posts: [],
            friends: [],
            postLikes: [2],
            postDislikes: [5],
            commentLikes: [1],
            commentDislikes: [1]
        }];

    const testMember: UserReturn = {
        id: 2,
        name: 'Kay',
        email: 'Kay@da.com',
        status: {
            isBanned: false,
            description: 'description',
        },
        posts: [],
        friends: [],
        postLikes: [3],
        postDislikes: [2],
        commentLikes: [2],
        commentDislikes: [4]
    };

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: HttpClient,
                useValue: http,
            }
        ]
    }));

    it('should be created', () => {
        const service: MembersDataService = TestBed.get(MembersDataService);
        expect(service).toBeTruthy();
    });

    it('getAllMembers should return an observable list of objects', () => {

        http.get.and.returnValue(of(testMembers));
        const service: MembersDataService = TestBed.get(MembersDataService);
        service.getAllMembers().subscribe(
            membersData => {
                expect(membersData[0].id).toEqual(1);
                expect(membersData[0].name).toEqual('Yoda');
                expect(membersData[0].email).toEqual('yoda@yoda.com');
                expect(membersData[0].status).toEqual({ isBanned: false, description: 'description' });
                expect(membersData[0].posts).toEqual([]);
                expect(membersData[0].friends).toEqual([]);
                expect(membersData[0].postLikes).toEqual([2]);
                expect(membersData[0].postDislikes).toEqual([5]);
                expect(membersData[0].commentLikes).toEqual([1]);
                expect(membersData[0].commentDislikes).toEqual([1]);
            }
        );

    });

    it('getAllMembers should call http.get once', () => {
        const service: MembersDataService = TestBed.get(MembersDataService);
        http.get.calls.reset();
        service.getAllMembers().subscribe(() => expect(http.get).toHaveBeenCalledTimes(1));
    });

    it('getMember should return an observable of an object', () => {

        http.get.and.returnValue(of(testMember));
        const service: MembersDataService = TestBed.get(MembersDataService);
        const memberId = 2;
        service.getMember(memberId).subscribe(
            memberData => {
                expect(memberData.id).toEqual(2);
                expect(memberData.name).toEqual('Kay');
                expect(memberData.email).toEqual('Kay@da.com');
                expect(memberData.status).toEqual({ isBanned: false, description: 'description' });
                expect(memberData.posts).toEqual([]);
                expect(memberData.friends).toEqual([]);
                expect(memberData.postLikes).toEqual([3]);
                expect(memberData.postDislikes).toEqual([2]);
                expect(memberData.commentLikes).toEqual([2]);
                expect(memberData.commentDislikes).toEqual([4]);
            }
        );

    });

    it('getMember should call http.get once', () => {
        const service: MembersDataService = TestBed.get(MembersDataService);
        http.get.calls.reset();
        const memberId = 2;
        service.getMember(memberId).subscribe(() => expect(http.get).toHaveBeenCalledTimes(1));
    });

    it('editMemberById should return the appropriate message after updating the member', () => {

        http.put.and.returnValue(of({ message: 'Mock update completed.' }));

        const service: MembersDataService = TestBed.get(MembersDataService);
        const memberId = 2;
        const member: UpdateProfile = { email: 'mock@mock.com', password: 'mock' };

        service.editMemberById(memberId, member).subscribe(
            (result) => {
                expect(result.message).toBe('Mock update completed.');
            }
        );

    });

    it('editMembeById should call http.put once', () => {
        const service: MembersDataService = TestBed.get(MembersDataService);
        http.put.calls.reset();

        const memberId = 2;
        const member: UpdateProfile = { email: 'mock@mock.com', password: 'mock' };

        service.editMemberById(memberId, member).subscribe(
            () => expect(http.put).toHaveBeenCalledTimes(1)
        );
    });

    it('addFriendById should return the appropriate message after adding a friend', () => {
        http.post.and.returnValue(of({ message: 'Mockup friend added successfully!' }));

        const service: MembersDataService = TestBed.get(MembersDataService);
        const testFriendId = 2;
        service.addFriendById(testMember, testFriendId).subscribe(
            (result) => {
                expect(result.message).toBe('Mockup friend added successfully!');
            }
        );
    });

    it('addFriendById should call http.post once', () => {
        const service: MembersDataService = TestBed.get(MembersDataService);
        http.post.calls.reset();

        const testFriendId = 2;

        service.addFriendById(testMember, testFriendId).subscribe(
            () => expect(http.post).toHaveBeenCalledTimes(1)
        );
    });

    it('unfriendById should return the appropriate message after removing a friend', () => {
        http.put.and.returnValue(of({ message: 'Mockup friend removed successfully!' }));

        const service: MembersDataService = TestBed.get(MembersDataService);
        const testUnfriendedId = 2;
        service.unfriendById(testMember, testUnfriendedId).subscribe(
            (result) => {
                expect(result.message).toBe('Mockup friend removed successfully!');
            }
        );
    });

    it('unfriendById should call http.put once', () => {
        const service: MembersDataService = TestBed.get(MembersDataService);
        http.put.calls.reset();

        const testUnfriendedId = 2;

        service.unfriendById(testMember, testUnfriendedId).subscribe(
            () => expect(http.put).toHaveBeenCalledTimes(1)
        );
    });

    it('banMember should return the appropriate message after a member is banned', () => {
        http.put.and.returnValue(of({ message: 'Mockup member has been banned successfully!' }));

        const service: MembersDataService = TestBed.get(MembersDataService);
        const testDescription: BanDescription = { content: 'ban description' };
        const testMemberId = 2;
        service.banMember(testDescription, testMemberId).subscribe(
            (result) => {
                expect(result.message).toBe('Mockup member has been banned successfully!');
            }
        );
    });

    it('banMember should call http.put once', () => {
        const service: MembersDataService = TestBed.get(MembersDataService);
        http.put.calls.reset();

        const testMemberId = 2;
        const testDescription: BanDescription = { content: 'ban description' };
        service.banMember(testDescription, testMemberId).subscribe(
            () => expect(http.put).toHaveBeenCalledTimes(1)
        );
    });

    it('deleteMember should return the appropriate message after a member is deleted', () => {

        http.delete.and.returnValue(of({ message: 'Mockup member has been deleted successfully!' }));
        const service: MembersDataService = TestBed.get(MembersDataService);
        const memberId = 2;
        service.deleteMember(memberId).subscribe(
            (result) => {
                expect(result.message).toBe('Mockup member has been deleted successfully!');
            }
        );
    });

    it('deleteMember should call http.delete once', () => {
        const service: MembersDataService = TestBed.get(MembersDataService);
        http.delete.calls.reset();

        const memberId = 2;
        service.deleteMember(memberId).subscribe(
            () => expect(http.delete).toHaveBeenCalledTimes(1)
        );
    });

});
