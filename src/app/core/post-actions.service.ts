import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Posst } from '../models/post';

@Injectable({
    providedIn: 'root'
})

export class PostsActionsService {

    public constructor(private readonly http: HttpClient) {}

    public upvotePost(id: number): Observable<Posst> {
        return this.http.put<Posst>(`http://localhost:3002/posts/${id}/votes`, {});
    }

    public downvotePost(id: number): Observable<Posst> {
        return this.http.put<Posst>(`http://localhost:3002/posts/${id}/downvotes`, {});
    }

    public flagPost(id: number): Observable<Posst> {
        return this.http.put<Posst>(`http://localhost:3002/posts/${id}/flag`, {});
    }

    public lockPost(id: number): Observable<Posst> {
        return this.http.put<Posst>(`http://localhost:3002/posts/${id}/lock`, {});
    }
}
