import { UserLogin } from './../models/user-login';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';
import { UserRegister } from '../models/user-register';
import { StorageService } from './storage.service';
import { UserReturn } from '../models/user-return';
import { JwtHelperService } from '@auth0/angular-jwt';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class AuthService {
  public prevVotes: any;
  private readonly isLoggedInSubject$ = new BehaviorSubject<string | null>(this.username);

  public constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly toastr: ToastrService,
    private readonly jwtHelper: JwtHelperService,
  ) {

    if (this.isTokenExpired) {
      this.cleanStorageAtLogout();
    }
  }

  public get isLoggedIn$(): Observable<string | null> {
    return this.isLoggedInSubject$.asObservable();
  }

  private get isTokenExpired(): boolean {
    const token = this.storage.getItem('token');
    if (token) {
      return this.jwtHelper.isTokenExpired();
    }
    return false;
  }

  private get username(): string | null {
    const token = this.storage.getItem('token');
    const username = this.storage.getItem('username') || '';
    if (token) {
      return username;
    }
    return null;
  }

  public getUserById(id: string): Observable<UserReturn> {
    return this.http.get<UserReturn>(`http://localhost:3002/users/${id}`);
  }

  public retrieveMemberId() {
    const token = this.storage.getItem('token');
    const id = this.storage.getItem('id') || '';
    if (token) {
      return +id;
    }
    return null;
  }

  public register(user: UserRegister): Observable<any> {
    return this.http.post('http://localhost:3002/registration', user);
  }

  public login(user: UserLogin): Observable<any> {

    return this.http.post('http://localhost:3002/session', user).pipe(
      tap(data => {
        const tokenExpirationTime =
          this.jwtHelper.getTokenExpirationDate(data.token).getTime() / 1000; // gives exp. date of token in seconds
        const startTime = (new Date()).getTime() / 1000; // gives date today in seconds
        const tokenValidityPeriod = tokenExpirationTime - startTime;

        setTimeout(() => this.logOutWhenTokenExpiresCallback(), tokenValidityPeriod * 1000);
        this.isLoggedInSubject$.next(data.user.name);
        this.storage.setItem('token', data.token);
        this.storage.setItem('id', data.user.id);
        this.storage.setItem('username', data.user.name);
        this.storage.setItem('role', data.user.role);
      })
    );
  }

  public logout(): Observable<any> {
    return this.http.delete('http://localhost:3002/session').pipe(
      tap(() => {
        this.cleanStorageAtLogout();
      })
    );
  }

  public cleanStorageAtLogout(): void {
    this.storage.removeItem('token');
    this.storage.removeItem('id');
    this.storage.removeItem('username');
    this.storage.removeItem('role');
    this.isLoggedInSubject$.next(null);
  }

  public logOutWhenTokenExpiresCallback(): void {
    this.cleanStorageAtLogout();
    this.toastr.success('Your session has expired. You must log in again in order to continue!');
  }
}
