import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserReturn } from '../models/user-return';
import { UpdateProfile } from '../models/update-profile';
import { BanDescription } from '../models/ban-description';

@Injectable({
    providedIn: 'root'
  })
export class MembersDataService {
    public constructor(private readonly http: HttpClient) { }

    public getAllMembers(): Observable<UserReturn[]> {
        const url = 'http://localhost:3002/users';

        return this.http.get<UserReturn[]>(url);
    }

    public getMember(id: number): Observable<UserReturn> {
        return this.http.get<UserReturn>(`http://localhost:3002/users/${id}`);
    }

    public editMemberById(id: number, member: UpdateProfile): Observable<{ message: string }> {
        return this.http.put<{ message: string }>((`http://localhost:3002/users/${id}`), member);
    }

    public addFriendById(friendedBy: UserReturn, friendId: number): Observable<{ message: string }> {
        return this.http.post<{ message: string }>((`http://localhost:3002/users/${friendId}/friends`), friendedBy);
    }

    public unfriendById(unfriendedBy: UserReturn, unfriendedId: number ): Observable<{ message: string }> {
        return this.http.put<{ message: string }>((`http://localhost:3002/users/${unfriendedId}/friends`), unfriendedBy);
    }

    public banMember( description: BanDescription, id: number): Observable<{ message: string }> {
        return this.http.put<{ message: string }>(`http://localhost:3002/users/${id}/banstatus`, description);
    }

    public deleteMember(id: number): Observable<{ message: string }> {
        return this.http.delete<{ message: string }>(`http://localhost:3002/users/${id}`);
    }


}
