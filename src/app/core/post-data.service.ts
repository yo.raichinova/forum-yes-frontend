import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Posst } from '../models/post';
import { CreatePost } from '../models/create-post';

@Injectable()
export class PostsDataService {
    public constructor(private readonly http: HttpClient) {}

    public getAllPosts(text?: string): Observable<Posst[]> {
        const url = text
            ? `http://localhost:3002/posts?text=${text}`
            : 'http://localhost:3002/posts';

        return this.http.get<Posst[]>(url);
    }

    public getPost(id: number): Observable<Posst> {
        return this.http.get<Posst>(`http://localhost:3002/posts/${id}`);
    }

    public createPost(post: CreatePost): Observable<Posst> {
        return this.http.post<Posst>('http://localhost:3002/posts', post);
    }

    public updatePost(id: number, post: CreatePost): Observable<{ message: string }> {
        return this.http.put<{message: string}>(`http://localhost:3002/posts/${id}`, post);
    }
    public deletePost(id: number): Observable<{ message: string }> {
        return this.http.delete<{ message: string }>(`http://localhost:3002/posts/${id}`);
    }
}
