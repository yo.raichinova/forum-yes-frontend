import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class CommentsActionsService {

    public constructor(private readonly http: HttpClient) { }

    public upvoteComment(postId: number, commentId: number): Observable<Comment> {
        return this.http.put<Comment>(`http://localhost:3002/posts/${postId}/comments/${commentId}/votes`, {});
    }

    public downvoteComment(postId: number, commentId: number): Observable<Comment> {
        return this.http.put<Comment>(`http://localhost:3002/posts/${postId}/comments/${commentId}/downvotes`, {});
    }

    public flagComment(postId: number, commentId: number): Observable<Comment> {
        return this.http.put<Comment>(`http://localhost:3002/posts/${postId}/comments/${commentId}/flag`, {});
    }

    public lockComment(postId: number, commentId: number): Observable<Comment> {
        return this.http.put<Comment>(`http://localhost:3002/posts/${postId}/comments/${commentId}/lock`, {});
    }
}
