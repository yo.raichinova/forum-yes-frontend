import { TestBed } from '@angular/core/testing';
import { PostsActionsService } from './post-actions.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { Posst } from '../models/post';


describe('PostsActionsService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['post', 'put', 'delete']);

    const testPost: Posst[] = [
        {
            id: 1,
            title: 'Title',
            content: 'Content',
            user: 'user',
            createdAt: new Date(),
            updatedAt: new Date(),
            likedBy: 6,
            dislikedBy: 3,
            comments: Comment[0],
            isFlagged: false,
            isLocked: false
        }];

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: HttpClient,
                useValue: http,
            },
        ]
    }));

    it('should be created', () => {
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        expect(service).toBeTruthy();
    });

    it('upvotePost should update the right postId', () => {
        http.put.and.returnValue(of(testPost));
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        const postId = 1;
        service.upvotePost(postId).subscribe(
            postData => {
                expect(postId).toEqual(1);
            }
        );

    });

    it('upvotePost should call http.put once', () => {
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        http.put.calls.reset();
        const postId = 1;
        service.upvotePost(postId).subscribe(
            () => expect(http.put).toHaveBeenCalledTimes(1)
        );
    });

    it('upvotePost should return the right number of likes', () => {
        http.put.and.returnValue(of(testPost));
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        const postId = 1;
        const likes = 6;
        service.upvotePost(postId).subscribe(
            postData => {
                expect(likes).toEqual(6);
            }
        );

    });

    it('downvotePost should update the right postId', () => {
        http.put.and.returnValue(of(testPost));
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        const postId = 1;
        service.downvotePost(postId).subscribe(
            postData => {
                expect(postId).toEqual(1);
            }
        );

    });

    it('downvotePost should call http.put once', () => {
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        http.put.calls.reset();
        const postId = 1;
        service.downvotePost(postId).subscribe(
            () => expect(http.put).toHaveBeenCalledTimes(1)
        );
    });

    it('downvotePost should return the right number of dislikes', () => {
        http.put.and.returnValue(of(testPost));
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        const postId = 1;
        const dislikes = 3;
        service.downvotePost(postId).subscribe(
            postData => {
                expect(dislikes).toEqual(3);
            }
        );

    });

    it('flagPost should update the right postId', () => {
        http.put.and.returnValue(of(testPost));
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        const postId = 1;
        service.flagPost(postId).subscribe(
            postData => {
                expect(postId).toEqual(1);
            }
        );

    });

    it('flagPost should call http.put once', () => {
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        http.put.calls.reset();
        const postId = 1;
        service.flagPost(postId).subscribe(
            () => expect(http.put).toHaveBeenCalledTimes(1)
        );
    });

    it('flagPost should return the right state of the post', () => {
        http.put.and.returnValue(of(testPost));
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        const postId = 1;
        const lock = false;
        service.flagPost(postId).subscribe(
            postData => {
                expect(lock).toEqual(false);
            }
        );
    });

    it('lockPost should update the right postId', () => {
        http.put.and.returnValue(of(testPost));
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        const postId = 1;
        service.lockPost(postId).subscribe(
            postData => {
                expect(postId).toEqual(1);
            }
        );

    });

    it('lockPost should call http.put once', () => {
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        http.put.calls.reset();
        const postId = 1;
        service.lockPost(postId).subscribe(
            () => expect(http.put).toHaveBeenCalledTimes(1)
        );
    });

    it('lockPost should return the right state of the post', () => {
        http.put.and.returnValue(of(testPost));
        const service: PostsActionsService = TestBed.get(PostsActionsService);
        const postId = 1;
        const lock = false;
        service.lockPost(postId).subscribe(
            postData => {
                expect(lock).toEqual(false);
            }
        );
    });

});
