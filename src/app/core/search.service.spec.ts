import { TestBed } from '@angular/core/testing';
import { SearchService } from './search.service';

describe('SearchService', () => {

  beforeEach(() => TestBed.configureTestingModule({
    providers: []
  }));

  it('should be created', () => {
    const service: SearchService = TestBed.get(SearchService);
    expect(service).toBeTruthy();
  });

  it('emitSearch should update the subject', () => {
    const service: SearchService = TestBed.get(SearchService);
    const searchTestInput = 'testInput';

    service.emitSearch(searchTestInput);
    service.search$.subscribe(
        (res) => expect(res).toBe('testInput')
        );
    });
});
