import { TestBed } from '@angular/core/testing';
import { CommentsDataService } from './comment-data.service';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

describe('CommentsDataService', () => {
    const http = jasmine.createSpyObj('HttpClient', ['post', 'put', 'delete']);

    beforeEach(() => TestBed.configureTestingModule({
        providers: [
            {
                provide: HttpClient,
                useValue: http,
            },
        ]
    }));

    it('should be created', () => {
        const service: CommentsDataService = TestBed.get(CommentsDataService);
        expect(service).toBeTruthy();
    });

    it('createComment should create new comment', () => {
        const testComment = new Comment();

        http.post.and.returnValue(of(testComment));

        const service: CommentsDataService = TestBed.get(CommentsDataService);
        const postId = 2;
        const commentCreate = { content: 'new test Comment' };
        service.createComment(postId, commentCreate).subscribe(
            (res) => {
                expect(res).toBe(testComment);
            }
        );

    });
    it('createComment should call http.post once', () => {
        const service: CommentsDataService = TestBed.get(CommentsDataService);
        http.post.calls.reset();

        const postId = 2;
        const commentCreate = { content: 'new test Comment' };

        service.createComment(postId, commentCreate).subscribe(
            () => expect(http.post).toHaveBeenCalledTimes(1)
        );
    });

    it('updateComment should return correct message after updating the comment', () => {

        http.put.and.returnValue(of({ message: 'Successful test update' }));

        const service: CommentsDataService = TestBed.get(CommentsDataService);
        const postId = 2;
        const commentId = 3;
        const testCommentUpdate = { content: 'test comment for update' };

        service.updateComment(postId, commentId, testCommentUpdate).subscribe(
            (res) => {
                expect(res.message).toBe('Successful test update');
            }
        );

    });

    it('updateComment should call http.put once', () => {
        const service: CommentsDataService = TestBed.get(CommentsDataService);
        http.put.calls.reset();

        const postId = 2;
        const commentId = 3;
        const testCommentUpdate = { content: 'test comment for update' };

        service.updateComment(postId, commentId, testCommentUpdate).subscribe(
            () => expect(http.put).toHaveBeenCalledTimes(1)
        );
    });

    it('deleteComment should return correct message after deleting the comment', () => {

        http.delete.and.returnValue(of({ message: 'Successful test delete' }));

        const service: CommentsDataService = TestBed.get(CommentsDataService);
        const postId = 2;
        const commentId = 3;

        service.deleteComment(postId, commentId).subscribe(
            (res) => {
                expect(res.message).toBe('Successful test delete');
            }
        );

    });

    it('deleteComment should call http.delete once', () => {
        const service: CommentsDataService = TestBed.get(CommentsDataService);
        http.delete.calls.reset();

        const postId = 2;
        const commentId = 3;

        service.deleteComment(postId, commentId).subscribe(
            () => expect(http.delete).toHaveBeenCalledTimes(1)
        );
    });

});
