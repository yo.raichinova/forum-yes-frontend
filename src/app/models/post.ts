import { Comment } from './comment';

export interface Posst {
    id: number;

    title: string;

    content: string;

    user: string;

    createdAt: Date;

    updatedAt ?: Date;

    likedBy: number;

    dislikedBy: number;

    comments: Comment[];

    isFlagged: boolean;

    isLocked: boolean;
}
