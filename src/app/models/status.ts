export interface Status {
    isBanned: boolean;
    description: string;
}
