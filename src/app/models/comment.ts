export interface Comment {
    id: number;

    content: string;

    user: string;

    createdAt: Date;

    updatedAt?: Date;

    likedBy: number;

    dislikedBy: number;

    isFlagged: boolean;

    isLocked: boolean;
}
