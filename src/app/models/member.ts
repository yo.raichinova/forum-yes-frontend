import { Posst } from './post';
import { UserReturn } from './user-return';

export interface Member {
  id: number;
  name: string;
  posts: Posst[];
  friends: UserReturn[];
}
