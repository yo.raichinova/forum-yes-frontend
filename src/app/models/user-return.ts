import { Posst } from './post';
import { Member } from './member';
import { Status } from './status';

export interface UserReturn {
    id: number;
    name: string;
    email: string;
    status: Status;
    posts: Posst[];
    friends: Member[];
    postLikes: number[];
    postDislikes: number[];
    commentLikes: number[];
    commentDislikes: number[];
}
