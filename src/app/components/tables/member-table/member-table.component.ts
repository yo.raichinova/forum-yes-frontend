import { Component, OnInit, Input } from '@angular/core';
import { UserReturn } from '../../../../app/models/user-return';

@Component({
  selector: 'app-member-table',
  templateUrl: './member-table.component.html',
  styleUrls: ['./member-table.component.scss']
})
export class MemberTableComponent implements OnInit {

  @Input() public members: UserReturn[];

  constructor() { }

  ngOnInit() {}

}
