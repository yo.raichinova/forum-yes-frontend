import { Component, OnInit, Input } from '@angular/core';
import { Posst } from '../../../../app/models/post';

@Component({
  selector: 'app-post-table',
  templateUrl: './post-table.component.html',
  styleUrls: ['./post-table.component.scss']
})
export class PostTableComponent implements OnInit {

  @Input() public posts: Posst[];

  constructor() { }

  ngOnInit() {
  }
}
