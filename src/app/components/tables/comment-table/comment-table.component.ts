import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../../../../app/models/comment';
import { UserReturn } from '../../../../app/models/user-return';

@Component({
  selector: 'app-comment-table',
  templateUrl: './comment-table.component.html',
  styleUrls: ['./comment-table.component.scss']
})
export class CommentTableComponent implements OnInit {

  @Input() public comments: Comment[];
  @Input() public loggedUser: UserReturn;
  @Input() public loggedUserName: string;
  @Input() public loggedUserRole: string;
  @Input() public postId: number;

  constructor() { }

  ngOnInit() {

  }

}
