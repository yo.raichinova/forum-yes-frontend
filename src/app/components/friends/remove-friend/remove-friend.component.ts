import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserReturn } from 'src/app/models/user-return';

@Component({
  selector: 'app-remove-friend',
  templateUrl: './remove-friend.component.html',
  styleUrls: ['./remove-friend.component.scss']
})
export class RemoveFriendComponent implements OnInit {
  @Input() member;
  @Input() currentlyLoggedMember;

  @Output() unfriend = new EventEmitter<UserReturn>();

  constructor() { }

  ngOnInit() {
  }

  public onUnfriendMemberButtonClick(): void {
    this.unfriend.emit(this.member);
  }


}
