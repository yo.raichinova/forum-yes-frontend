import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UserReturn } from 'src/app/models/user-return';

@Component({
  selector: 'app-add-friend',
  templateUrl: './add-friend.component.html',
  styleUrls: ['./add-friend.component.scss']
})
export class AddFriendComponent implements OnInit {
  @Input() member;
  @Input() currentlyLoggedMember;

  @Output() friend = new EventEmitter<UserReturn>();

  constructor() { }

  ngOnInit() {
  }

  public onFriendMemberButtonClick(): void {
    this.friend.emit(this.member);
  }


}
