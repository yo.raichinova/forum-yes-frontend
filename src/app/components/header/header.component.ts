import { Component, Input, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/core/auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Input()
  public username = '';

  @Input()
  public isLoggedIn = false;

  public isNavbarCollapsed = true;

  @Output()
  public logout = new EventEmitter<undefined>();

  public constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
  ) { }

  public get searchCallback(): () => void {
    return this.searchNavigationCallback.bind(this);
  }

  private searchNavigationCallback(): void {
    this.router.navigate(['/posts']);
  }

  public triggerLogout() {
    this.logout.emit();
  }
}
