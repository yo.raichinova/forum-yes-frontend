import { Component, OnInit, Input } from '@angular/core';
import { Comment } from '../../../app/models/comment';
import { UserReturn } from '../../../app/models/user-return';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { CommentsDataService } from 'src/app/core/comment-data.service';
import { CommentsActionsService } from 'src/app/core/comment-actions.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  @Input() public comment: Comment;
  @Input() public loggedUser: UserReturn;
  @Input() public loggedUserName: string;
  @Input() public loggedUserRole: string;
  @Input() public postId: number;

  public showEditButton = false;
  public showVotesIcons = true;
  public disableLikeButton = false;
  public disableDislikeButton = false;

  constructor(
    private readonly commentsDataService: CommentsDataService,
    private readonly commentsActionsService: CommentsActionsService,
    private readonly toastr: ToastrService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    if (this.comment.user === this.loggedUserName) {
      this.showEditButton = true;
      this.showVotesIcons = false;
    }
    if (this.loggedUserRole === 'admin') {
      this.showEditButton = true;
    }

    if (this.loggedUser.commentLikes.includes(this.comment.id)) {
      this.disableLikeButton = true;
    }
    if (this.loggedUser.commentDislikes.includes(this.comment.id)) {
      this.disableDislikeButton = true;
    }
  }

  public triggerDeleteComment() {
    this.commentsDataService.deleteComment(this.postId, this.comment.id).subscribe(
      (data) => {
        this.toastr.success(`${data.message}`);
        this.router.navigate(['/posts']);
      },
      (error) => {
        this.toastr.error(error.message);
      }
    );
  }
  public triggerUpvoteComment() {
    this.commentsActionsService.upvoteComment(this.postId, this.comment.id).subscribe(
      () => {
        this.comment.likedBy++;
        if (this.disableDislikeButton) {
          this.comment.dislikedBy--;
          this.disableDislikeButton = false;
        }
        this.disableLikeButton = true;
      },
      (error) => {
        this.toastr.error(error.message);
      }
    );
  }
  public triggerDownvoteComment() {
    this.commentsActionsService.downvoteComment(this.postId, this.comment.id).subscribe(
      () => {
        this.comment.dislikedBy++;
        if (this.disableLikeButton) {
          this.comment.likedBy--;
          this.disableLikeButton = false;
        }
        this.disableDislikeButton = true;
      },
      (error) => {
        this.toastr.error(error.message);
      }
    );
  }

}
