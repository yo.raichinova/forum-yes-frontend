import { Component, OnInit, Input } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AppNgbdModalCommentEditContentComponent } from './modal-content/modal-content';

@Component({
  selector: 'app-ngbd-modal-comment-edit-component',
  templateUrl: './modal-component.html'
})
export class AppNgbdModalCommentEditComponent {

  @Input() public postId: number;
  @Input() public commentId: number;
  @Input() public commentContent: string;

  constructor(private modalService: NgbModal) { }

  open() {
    const modalRef = this.modalService.open(AppNgbdModalCommentEditContentComponent);
    modalRef.componentInstance.postId = this.postId;
    modalRef.componentInstance.commentId = this.commentId;
    modalRef.componentInstance.commentContent = this.commentContent;
  }
}
