import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CommentsDataService } from '../../../../../app/core/comment-data.service';
import { CreateComment } from '../../../../../app/models/create-comment';


@Component({
  selector: 'app-ngbd-modal-comment-edit-content',
  template: `
    <form [formGroup]="this.formGroup" (validSubmit)="onSubmit(this.formGroup.value)">
  <div class="modal-header">
    <h4 class="modal-title">Please write your comment here!</h4>
    <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div>
      <label for="content">Content</label>
      <textarea [value]="commentContent" type="text" class="form-control"
      id="content-input" rows="3" placeholder="Content" formControlName="content" >
      </textarea>
    </div>
     <div *ngIf="this.formGroup.controls.content.invalid && this.formGroup.controls.content.dirty" class="alert alert-danger">
        <div *ngIf="this.formGroup.controls.content.errors.required">
          You cannot send an empty comment!
        </div>

        <div *ngIf="this.formGroup.controls.content.errors.minlength">
          Comment must be at least 2 characters long.
        </div>
     </div>
  </div>
  <div class="modal-footer">
    <button type="submit" class="btn btn-outline-success">Submit</button>
    <button type="button" class="btn btn-outline-dark" (click)="activeModal.close('Close click')">Close</button>
  </div>
</form>
  `
})
export class AppNgbdModalCommentEditContentComponent implements OnInit {

  @Input() public postId: number;
  @Input() public commentId: number;
  @Input() public commentContent: string;
  public formGroup: FormGroup;

  constructor(
    public activeModal: NgbActiveModal,
    private readonly commentsDataService: CommentsDataService,
    private readonly toastr: ToastrService,
    private readonly router: Router,
    private readonly formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.formGroup = new FormGroup({
      content: new FormControl(this.commentContent, [
        Validators.required,
        Validators.minLength(2)
      ]),
    });
  }

  onSubmit(comment) {
    this.activeModal.close('Submit');
    this.commentsDataService.updateComment(this.postId, this.commentId, comment).subscribe(
      (result) => {
        this.toastr.success(`${result.message}`);
        setTimeout(() => location.reload(), 1500);
        //  this.router.navigate(['/posts', this.postId]);
      },
      () => {
        this.toastr.error('Comment could not be edited.');
        this.router.navigate(['/posts', this.postId]);
      }
    );
  }
}
