import { of, throwError, Observable } from 'rxjs';
import { SharedModule } from '../../../app/shared/shared.module';
import { TestBed, async } from '@angular/core/testing';
import { CommentsDataService } from '../../../app/core/comment-data.service';
import { CommentsActionsService } from '../../../app/core/comment-actions.service';
import { CommentComponent } from './comment.component';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Comment } from '../../../app/models/comment';
import { UserReturn } from '../../../app/models/user-return';
import { PostsActionsService } from 'src/app/core/post-actions.service';

describe('CommentComponent', () => {
    let fixture;
    const commentsDataService = jasmine.createSpyObj('CommentsDataService', ['createComment', 'updateComment', 'deleteComment']);
    const commentsActionsService = jasmine.createSpyObj('CommentsActionsService', [
        'upvoteComment',
        'downvoteComment',
        'flagComment',
        'lockComment'
    ]);
    const toastr = jasmine.createSpyObj('ToastrService', ['success', 'error']);
    const router = jasmine.createSpyObj('Router', ['navigate']);

    const loggedUserName = 'testName';
    const comment: Comment = {
        id: 1,
        content: 'test comment',
        user: 'testName',
        createdAt: new Date(),
        updatedAt: new Date(),
        likedBy: 3,
        dislikedBy: 1,
        isFlagged: false,
        isLocked: false
    };
    const loggedUserRole = 'member';
    const loggedUser: UserReturn = {
        id: 2,
        name: 'testName',
        email: 'test@mail.bg',
        posts: [],
        friends: [],
        postLikes: [],
        postDislikes: [],
        commentLikes: [1],
        commentDislikes: [3],
        status: {
            isBanned: false,
            description: 'testdescription',
        }

    };
    const postId = 32;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [],
            imports: [
                SharedModule,
            ],
            providers: [
                {
                    provide: CommentsDataService,
                    useValue: commentsDataService,
                },
                {
                    provide: CommentsActionsService,
                    useValue: commentsActionsService,
                },
                {
                    provide: ToastrService,
                    useValue: toastr,
                },
                {
                    provide: Router,
                    useValue: router,
                },
            ]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });

    it('should create the CommentComponent', () => {
        fixture = TestBed.createComponent(CommentComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });

    it('should initialize with the correct data for showing/hiding the vote and edit icons', async () => {

        fixture = TestBed.createComponent(CommentComponent);
        const app = fixture.debugElement.componentInstance;
        app.comment = comment;
        app.loggedUser = loggedUser;
        app.loggedUserName = loggedUserName;
        app.loggedUserRole = loggedUserRole;
        await fixture.detectChanges();
        expect(app.showEditButton).toBe(true);
        expect(app.showVotesIcons).toBe(false);
        expect(app.disableLikeButton).toBe(true);
        expect(app.disableDislikeButton).toBe(false);
    });

    it('should call toastr.success and router.navigate when comment is deleted successfully', async () => {

        fixture = TestBed.createComponent(CommentComponent);
        const app = fixture.debugElement.componentInstance;

        app.comment = comment;
        app.loggedUser = loggedUser;
        app.loggedUserName = loggedUserName;
        app.loggedUserRole = loggedUserRole;
        app.postId = postId;
        const testError = new Error();
        commentsDataService.deleteComment.and.returnValue(of({ message: 'test' }));
        await fixture.detectChanges();
        app.triggerDeleteComment();

        expect(toastr.success).toHaveBeenCalledTimes(1);
        expect(toastr.success).toHaveBeenCalledWith('test');
        expect(router.navigate).toHaveBeenCalled();
    });

    it('should call toastr.error when comment was not deleted successfully', async () => {

        fixture = TestBed.createComponent(CommentComponent);
        const app = fixture.debugElement.componentInstance;

        app.comment = comment;
        app.loggedUser = loggedUser;
        app.loggedUserName = loggedUserName;
        app.loggedUserRole = loggedUserRole;
        app.postId = postId;

        commentsDataService.deleteComment.and.returnValue(throwError({ message: '404' }));
        // commentsDataService.deleteComment.and.callFake(() => {
        //     return throwError(new Error('Fake error'));
        // });
        await fixture.detectChanges();
        app.triggerDeleteComment();

        expect(toastr.error).toHaveBeenCalledTimes(1);
        expect(toastr.error).toHaveBeenCalledWith('404');
    });

    it('upvoteComment method should return correct disableDislikeButton value when comment was upvoted successfully', async () => {

        fixture = TestBed.createComponent(CommentComponent);
        const app = fixture.debugElement.componentInstance;

        app.comment = comment;
        app.loggedUser = loggedUser;
        app.loggedUserName = loggedUserName;
        app.loggedUserRole = loggedUserRole;
        app.postId = postId;
        app.disableDislikeButton = true;

        // commentsDataService.deleteComment.and.returnValue(throwError('Error occurred'));
        commentsActionsService.upvoteComment.and.returnValue(of('test'));

        await fixture.detectChanges();
        app.triggerUpvoteComment();

        expect(app.disableDislikeButton).toBe(false);
    });

    it('upvoteComment method should call toastr.error when comment was not upvoted successfully', async () => {

        fixture = TestBed.createComponent(CommentComponent);
        const app = fixture.debugElement.componentInstance;

        app.comment = comment;
        app.loggedUser = loggedUser;
        app.loggedUserName = loggedUserName;
        app.loggedUserRole = loggedUserRole;
        app.postId = postId;
        app.disableDislikeButton = true;

        toastr.error.calls.reset();

        commentsActionsService.upvoteComment.and.returnValue(throwError({ message: '404' }));

        await fixture.detectChanges();
        app.triggerUpvoteComment();

        expect(toastr.error).toHaveBeenCalledTimes(1);
        expect(toastr.error).toHaveBeenCalledWith('404');
    });
    it('downvoteComment method should return correct disableLikeButton value when comment was downvoted successfully', async () => {

        fixture = TestBed.createComponent(CommentComponent);
        const app = fixture.debugElement.componentInstance;

        app.comment = comment;
        app.loggedUser = loggedUser;
        app.loggedUserName = loggedUserName;
        app.loggedUserRole = loggedUserRole;
        app.postId = postId;
        app.disableLikeButton = true;

        // commentsDataService.deleteComment.and.returnValue(throwError('Error occurred'));
        commentsActionsService.downvoteComment.and.returnValue(of('test'));

        await fixture.detectChanges();
        app.triggerDownvoteComment();

        expect(app.disableLikeButton).toBe(false);
    });
    it('downvoteComment method should call toastr.error when comment was not downvoted successfully', async () => {

        fixture = TestBed.createComponent(CommentComponent);
        const app = fixture.debugElement.componentInstance;

        app.comment = comment;
        app.loggedUser = loggedUser;
        app.loggedUserName = loggedUserName;
        app.loggedUserRole = loggedUserRole;
        app.postId = postId;
        app.disableLikeButton = true;

        toastr.error.calls.reset();

        commentsActionsService.downvoteComment.and.returnValue(throwError({ message: '404' }));

        await fixture.detectChanges();
        app.triggerDownvoteComment();

        expect(toastr.error).toHaveBeenCalledTimes(1);
        expect(toastr.error).toHaveBeenCalledWith('404');
    });
});
