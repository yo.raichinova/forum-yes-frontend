import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-flag',
  templateUrl: './flag.component.html',
  styleUrls: ['./flag.component.scss']
})
export class FlagComponent implements OnInit {

  @Input() public isPostFlagged: boolean;

  @Output() public flag = new EventEmitter<undefined>();

  constructor() { }

  ngOnInit() {
  }

  public onFlagIconClick(): void {
    this.flag.emit();
    // this.isPostFlagged = !this.isPostFlagged;
  }
}
