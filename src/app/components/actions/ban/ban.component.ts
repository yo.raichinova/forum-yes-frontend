import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-ban',
  templateUrl: './ban.component.html',
  styleUrls: ['./ban.component.scss']
})
export class BanComponent {

  @Output() public ban = new EventEmitter<undefined>();

  public onBanIconClick(): void {
    this.ban.emit();
  }

}
