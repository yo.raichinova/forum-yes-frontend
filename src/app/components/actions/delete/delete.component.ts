import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss']
})
export class DeleteComponent {

  @Output() public delete = new EventEmitter<undefined>();

  public onDeleteIconClick(): void {
    this.delete.emit();
  }

}
