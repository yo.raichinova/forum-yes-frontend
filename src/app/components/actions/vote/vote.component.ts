import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.scss']
})
export class VoteComponent implements OnInit {

  @Input() public isLikeIconDisabled: boolean;
  @Input() public isDislikeIconDisabled: boolean;
  @Input() public likesNum: number;
  @Input() public dislikesNum: number;
  @Output() public upvote = new EventEmitter<undefined>();
  @Output() public downvote = new EventEmitter<undefined>();

  public onUpvoteIconClick(): void {
    this.upvote.emit();
  }
  public onDownvoteIconClick(): void {
    this.downvote.emit();
  }

  constructor() { }

  ngOnInit() {
  }

}
