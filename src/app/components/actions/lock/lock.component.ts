import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-lock',
  templateUrl: './lock.component.html',
  styleUrls: ['./lock.component.scss']
})
export class LockComponent implements OnInit {

  @Input() public icon: boolean;

  @Output() public lock = new EventEmitter<undefined>();

  constructor() { }

  ngOnInit() {
  }

  public onLockIconClick(): void {
    this.lock.emit();
    this.icon = !this.icon;
  }
}
