import { Component } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({selector: 'app-ngbd-carousel-basic', templateUrl: './carousel-basic.html', providers: [NgbCarouselConfig]})
export class NgbdCarouselBasicComponent {
  public imageOne = '../../../assets/media/1.jpg';
  public imageTwo = '../../../assets/media/2.jpg';
  public imageThree = '../../../assets/media/3.jpg';

  constructor(config: NgbCarouselConfig) {
    // customize default values of carousels used by this component tree
    config.interval = 3000;
    config.pauseOnHover = false;
  }

}
