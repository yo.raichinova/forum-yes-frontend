import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './components/home/home.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { ServerErrorInterceptor } from './interceptors/server-error-interceptor';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ParticlesComponent } from './components/header/particles/particles.component';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { MemberLoggedComponent } from './members/member-logged/member-logged.component';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { NgbdCarouselBasicComponentModule } from './components/carousel-basic/carousel-basic.module';
import { JwtModule } from '@auth0/angular-jwt';

export function tokenGetter() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    NotFoundComponent,
    ServerErrorComponent,
    ParticlesComponent,
    MemberLoggedComponent,
  ],
  imports: [
    BrowserModule,
    SharedModule,
    CoreModule,
    AppRoutingModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    CommonModule,
    ToastrModule.forRoot({
      timeOut: 4000,
      extendedTimeOut: 1000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      easing: 'ease-in',
      easeTime: 500,
      progressBar: true,
      progressAnimation: 'increasing',
      maxOpened: 2,
      newestOnTop: true,
    }),
    NgBootstrapFormValidationModule.forRoot(),
    NgbdCarouselBasicComponentModule,
    JwtModule.forRoot({
      config: {
        tokenGetter,
        whitelistedDomains: ['localhost:3002'],
        blacklistedRoutes: ['localhost:3002/login']
      }
    })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ServerErrorInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}


