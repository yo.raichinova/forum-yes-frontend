import { SearchService } from './../../core/search.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html'
})
export class SearchBarComponent implements OnInit {
  isActive: any;
  @Input() public defaultSearch = '';
  @Input() public searchCallback = () => {};

  public constructor(private readonly searchService: SearchService) {}

  public ngOnInit(): void {
    this.searchService.emitSearch(this.defaultSearch);
  }

  public keyBoardEvent(event) {
    if (event.keyCode === 13) {
      // this.searchButtonClick()
    }
  }

  public searchButtonClick(search: string, event: KeyboardEvent): void {

    this.searchService.emitSearch(search);
    this.searchCallback();
  }
}
