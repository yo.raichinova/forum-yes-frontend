import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DeleteComponent } from '../components/actions/delete/delete.component';
import { VoteComponent } from '../components/actions/vote/vote.component';
import { FlagComponent } from '../components/actions/flag/flag.component';
import { LockComponent } from '../components/actions/lock/lock.component';
import { PostTableComponent } from '../components/tables/post-table/post-table.component';
import { MemberTableComponent } from '../components/tables/member-table/member-table.component';
import { CommentTableComponent } from '../components/tables/comment-table/comment-table.component';
import { RouterModule } from '@angular/router';
import { CommentComponent } from '../components/comment/comment.component';
import { AppNgbdModalCommentEditComponent } from '../components/comment/modal-edit-comment/modal-component';
import { AppNgbdModalCommentEditContentComponent } from '../components/comment/modal-edit-comment/modal-content/modal-content';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgBootstrapFormValidationModule } from 'ng-bootstrap-form-validation';
import { BanComponent } from '../components/actions/ban/ban.component';


@NgModule({
  declarations: [
    SearchBarComponent,
    DeleteComponent,
    VoteComponent,
    FlagComponent,
    LockComponent,
    BanComponent,
    CommentTableComponent,
    PostTableComponent,
    MemberTableComponent,
    CommentComponent,
    AppNgbdModalCommentEditComponent,
    AppNgbdModalCommentEditContentComponent,
  ],
  imports: [CommonModule, RouterModule, NgbModule, HttpClientModule, FormsModule, ReactiveFormsModule, NgBootstrapFormValidationModule],
  exports: [
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgBootstrapFormValidationModule,
    HttpClientModule,
    SearchBarComponent,
    DeleteComponent,
    VoteComponent,
    FlagComponent,
    LockComponent,
    BanComponent,
    CommentTableComponent,
    PostTableComponent,
    MemberTableComponent,
    CommentComponent,
    AppNgbdModalCommentEditComponent,
    AppNgbdModalCommentEditContentComponent,
  ],
  entryComponents: [AppNgbdModalCommentEditContentComponent]
})
export class SharedModule { }
